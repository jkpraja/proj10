
variable "region" {
  default = "us-east-1"
}

variable "availability_zone" {
    default = "us-east-1a"
}

#variable "ami" {
#    default = "ami-0557a15b87f6559cf"
#}

variable "instance_type" {
    default = "t3.small"
}

variable "root_volume_size" {
   default = 30
}

variable "instance_count" {
    default = 1
}

variable "delete_on_termination" {
    default = true
}

#variable "volume_size" {
#    default = 20
#}

variable "volume_type" {
    default = "gp2"
}

variable "key_name" {
    default = "cilsy_server"
}

#variable "vpc_security_group_ids" {
#    default = ["sg-0d1878abf7b359496"]
#}

variable "associate_public_ip_address" {
    default = true
}

variable "tags" {
    type = map(string)
    default = {
        "name" = "projX"
        "purpose" = "hands-on"
        "env" = "dev"
    }
}