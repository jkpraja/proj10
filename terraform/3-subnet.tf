resource "aws_subnet" "public-us-east-1a" {
    vpc_id = aws_vpc.projX.id
    cidr_block = "10.0.0.0/19"
    availability_zone = "us-east-1a"

    tags = {
      "Name" = "public-us-east-1a"
    #  "kubernetes.io/role/internal-elb" = "1"
    #  "kubernetes.io/cluster/demo" = "owned"
    }
  
}