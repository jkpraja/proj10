data "aws_route53_zone" "c2loud" {
  name = "c2loud.com"
  private_zone = false
}

resource "aws_route53_record" "frontend" {
  zone_id = data.aws_route53_zone.c2loud.zone_id
  name    = "frontend.${data.aws_route53_zone.c2loud.name}"
  type    = "CNAME"
  ttl     = 300
  records = [aws_instance.cilist[0].public_dns]
}