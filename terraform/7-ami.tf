data "aws_ami" "ubuntu_ami" {
    filter {
      name = "name"
      values = ["*ubuntu-jammy-22.04-amd64-server-20230115*"]
      #values = ["docker*"]
    }

    #filter {
    #  name = "platform"
    #  values = ["ubuntu"]
    #}

    owners = ["amazon"]
    #owners = ["064887354011"]
    most_recent = true
}