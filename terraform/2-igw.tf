resource "aws_internet_gateway" "igw-projX" {
  vpc_id = aws_vpc.projX.id

  tags = {
    "Name" = "igw-projX"
  }
}