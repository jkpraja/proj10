resource "aws_instance" "cilist" {
  ami = data.aws_ami.ubuntu_ami.id
  #ami = "ami-0288e58b731662f25"
  instance_type = var.instance_type
  key_name = aws_key_pair.projX-key.key_name
  #vpc_security_group_ids = var.vpc_security_group_ids
  vpc_security_group_ids = aws_vpc_endpoint.endpoint_vpc.security_group_ids
  associate_public_ip_address = var.associate_public_ip_address
  count = var.instance_count
  
  root_block_device {
    volume_type           = var.volume_type
    volume_size           = var.root_volume_size
    delete_on_termination = var.delete_on_termination
  }
  
  #ebs_block_device {
  #  device_name = "/dev/sdb"
  #  volume_size = var.volume_size
  #  volume_type = var.volume_type
  #}

  subnet_id =  aws_subnet.public-us-east-1a.id

  tags = {
    Name = var.tags["name"]
    Purpose = var.tags["purpose"]
    Env = var.tags["env"]
  }

  #volume_tags = {
  #  Name = var.tags["name"]
  #  Purpose = var.tags["purpose"]
  #  Env = var.tags["env"]
  #}

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "curl -fsSL get.docker.com -o get-docker.sh",
      "chmod +x get-docker.sh",
      "sh get-docker.sh",
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo usermod -a -G docker ubuntu",
      "echo 'REACT_APP_BACKEND_URL=\"http://${self.public_ip}:5000\"' > fe.env",
      #"echo 'Wait till ssh is ready'"
      ]

    connection {
      host = self.public_ip
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/users/jkpraja/Dropbox/Ilmu/devops/aws/projX_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu -i ${self.public_ip}, --private-key ${"/Users/jkpraja/Dropbox/Ilmu/devops/aws/projX_rsa"} ../docker-install.yaml"
  }
}

output "cilist_ip" {
  value = aws_instance.cilist[0].public_ip
  #fe-env = "REACT_APP_BACKEND_URL=http://${aws_instance.cilist[0].public_ip}:5000/"
}