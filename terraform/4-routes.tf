resource "aws_route_table" "projX-rt-public" {
  vpc_id = aws_vpc.projX.id

  route {
    #carrier_gateway_id = ""
    cidr_block = "0.0.0.0/0"
    #core_network_arn = ""
    #destination_prefix_list_id = ""
    #egress_only_gateway_id = ""
    gateway_id = aws_internet_gateway.igw-projX.id
    #instance_id = ""
    #ipv6_cidr_block = ""
    #local_gateway_id = ""
    #nat_gateway_id = ""
    #network_interface_id = ""
    #transit_gateway_id = ""
    #vpc_endpoint_id = ""
    #vpc_peering_connection_id = ""
  }

  tags = {
    "Name" = "projX-rt-public"
  }  
}

resource "aws_route_table_association" "public-us-east-1a" {
  subnet_id = aws_subnet.public-us-east-1a.id
  route_table_id = aws_route_table.projX-rt-public.id
}