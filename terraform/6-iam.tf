resource "aws_iam_role" "projX-ec2" {
    name = "projX-ec2"

    assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        #Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }]
    })
}

resource "aws_iam_role_policy_attachment" "projX-AmazonEC2FullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  role = aws_iam_role.projX-ec2.name
}