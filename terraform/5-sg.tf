resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all traffic"
  vpc_id      = aws_vpc.projX.id

  ingress {
    #description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }


  tags = {
    Name = "allow_all"
  }
}

resource "aws_vpc_endpoint" "endpoint_vpc" {
vpc_id            = aws_vpc.projX.id
service_name      = "com.amazonaws.us-east-1.ec2"
vpc_endpoint_type = "Interface"

security_group_ids = [
    aws_security_group.allow_all.id,
]

#private_dns_enabled = true
}