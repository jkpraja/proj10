
# Deploy NodeJS Application using Terraform and Ansible

In this project, we will deploy an application that consists of frontend, backend and a MySQL database using terraform and ansible.


## Authors

- [@jkpraja](https://www.gitlab.com/jkpraja)


## Requirement
To accomplish this project, you'll need:
- an AWS Account
- a Docker Hub Account
## What To Do
In this case, we need to do several steps:
1. Prepare the resources using terraform
2. Prepare the deployment server using ansible
3. Deploy the application using ansible


### Resources Preparation using terraform
1. Create the 0-provider.tf
This file will be used to init the terraform to use aws as provider.

2. Create every resources need to launch an EC2 using terraform
In order to launch an EC2 instance, we'll need:
- VPC
- Internet gateway
- Subnet
- Routing Table
- Security Groups
- IAM
- AMI
- Key Pair to access deployment server

3. After above requirement fulfilled, then we can create an EC2 instance.

### Deployment Server Preparation using ansible
1. After launching EC2 instance, we will need to do local-exec provisioner and do command for ansible to do some tasks, such as:
- Docker Login operation
- And reset ssh connection in order to make changes to take effect

### Deploy application using ansible
1. Copy the required files to deploy app to deployment server, such as .env, and compose.yaml
2. Run docker compose up -d
3. Don't forget to update the backend URL in frontend .env file.

That's all, this project is completed.



